#!/bin/bash
# make sure this file's end-of-line encoding is in UNIX format (LF), not Windows (CR LF) or Mac (CR), or the script's execution will fail

# set variable for child processes; this will answer any prompt from installs/updates/upgrades with default answers and thus make these operations silent
export DEBIAN_FRONTEND="noninteractive"

# install prerequisites and ansible
sudo apt update -y && sudo apt install -y software-properties-common && sudo apt-add-repository -y ppa:ansible/ansible && sudo apt update -y && sudo apt install -y ansible

# generate keypair for use with future nodes, remove this if you prefer to generate keys later and/or manually
ssh-keygen -b 4096 -t rsa -f ~/.ssh/id_rsa -q -N ""

# launch our playbook. We will leave it on our control node, because if anything breaks in our environment we can just run it again locally, without having to re-create the node
ansible-playbook ansible/playbook.yml