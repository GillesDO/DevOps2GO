variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "private_key_path" {}
variable "key_name" {}
#This sets the AWS region we're using. It's also called "Europe (Frankfurt)" in the AWS Console. You can change this to suit your needs.
variable "region" {
  default = "eu-central-1"
}